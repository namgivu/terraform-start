resource "docker_image" "ubuntu" {
  name         = "namgivu/ubuntu-pipenv:18.04-3.7"
  keep_locally = false  //TODO what this for?
}

resource "docker_container" "ubuntu" {
  image = docker_image.ubuntu.latest  //TODO why :latest is required? it's confusing with latest tag of the docker image
  name  = "nn_tfcontainer"  // tf aka terraform ; nn aka namgivu
}
