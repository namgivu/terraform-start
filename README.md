# install & run
ref. https://learn.hashicorp.com/terraform/getting-started
```bash
sudo apt-get install unzip

# ref. https://www.terraform.io/downloads.html
download_url=https://releases.hashicorp.com/terraform/0.12.26/terraform_0.12.26_linux_amd64.zip

cd /tmp 
    wget $download_url -O $f && unzip terraform_0.12.26_linux_amd64.zip
    sudo mv terraform /usr/local/bin/
cd - >/dev/null

terraform --version
    
    # 0th run - spinup a ubuntu container via 
    terraform init
        # will have .terraform/ created; required plugin(s) be stored here under .terraform/plugins
    
        terraform apply --auto-approve
        docker ps | grep -E 'nn_tfcontainer|IMAGE'
            ss=`cat<<EOF
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
ea114bdd91f6        beb675725e6e        "/bin/sh -c 'tail -F…"   7 minutes ago       Up 7 minutes                            nn_tfcontainer
EOF`
            # remove the container
            terraform destroy --auto-approve
